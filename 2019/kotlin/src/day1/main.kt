package day1

import java.io.File
import kotlin.math.roundToInt

fun part1(input: String, calculate: (Int) -> Int): Int = File(input).useLines { it ->
    return it.toList().map {
        calculate(it.toInt())
    }.sum()
}

fun calculate(weight: Int): Int {
    return (weight / 3).toDouble().roundToInt() - 2
}

fun calculateRecursive(weight: Int): Int {
    var current = weight
    var total = 0
    while (current > 0) {
        current = calculate(current).also {
            if (it >= 0) {
                total += it
            }
        }
    }
    return total
}

fun main() {
    println(part1("d01p01.txt"){ calculate(it)})
    println(part1("d01p01.txt"){ calculateRecursive(it) })
}
