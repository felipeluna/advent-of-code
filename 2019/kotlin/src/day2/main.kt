package day2

import java.io.File

fun readFile(filename: String) = File(filename).readText().split(',').map { it.toInt() }.toMutableList()

fun calculate(list: MutableList<Int>) {
    loop@ for (i in list.indices step 4) {
        when (list[i]) {
            // next position + next next position = thirdpositions
            1 -> {
                list[list[i + 3]] = list[list[i + 1]] + list[list[i + 2]]
            }
            2 -> {
                list[list[i + 3]] = list[list[i + 1]] * list[list[i + 2]]

            }
            99 -> break@loop
        }
    }
}

fun findTheValue(list: List<Int>) {
    var noun = 0
    var verb = 0
   loop@ while (noun < 100) {
        while (verb < 100) {
            var n = list.toIntArray().copyOf().toMutableList()
            n[1] = noun
            n[2] = verb
            calculate(n)
            if (n[0] == 19690720) {
                val nounString = noun.toString().padStart(2,'0')
                val verbString = verb.toString().padStart(2, '0')
                println("$nounString$verbString")
                break@loop
            } else {
                verb++
            }
        }
        noun++
        verb=0
    }
}

fun main() {
    var aList = readFile("d02p01.txt")
    val secondList = aList.toIntArray().copyOf().toList()
    calculate(aList)
    println(aList[0])
    findTheValue(secondList)
}